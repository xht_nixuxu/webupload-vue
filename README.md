# webupload-vue

#### 介绍
使用vue+elementUI+webupload制作大文件分片上传插件，支持断点续传，文件MD5校验，分片md5校验，第一次上传成功，第二次上传同一个文件是就是文件秒传，从而达到秒传的目的
后端地址：https://gitee.com/hhjNavy/webupload.git，
插件在：src/components/common/upload/WebUpload.vue,
演示demo：src/components/page/sys/FileList.vue

#### 效果图

![输入图片说明](https://images.gitee.com/uploads/images/2020/0605/191626_64be700f_1981821.png "webupload1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0605/191637_c758cfb7_1981821.png "webuplaod2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0605/191646_c45b14f8_1981821.png "webupload3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0605/191715_979b0e8f_1981821.png "webupload4.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0605/191724_a1b5f447_1981821.png "webupload5.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0605/191733_eea72586_1981821.png "webupload6.png")


#### 效果演示

1.  克隆项目到本地，需要结合后台演示，所有需要克隆后端项目：https://gitee.com/hhjNavy/webupload.git
2.  把相关环境安装好，启动前端，后端
3.  访问 http://localhost:8081/#/filelist，就可以动手自己去点击体验了

#### 使用说明

1.  可以将src/components/common/upload下的WebUpload.vue插件复制拷贝到自己项目下（需要将src/assets/Uploader.swf拷贝到自己静态资源文件夹下），安装webupload、jquery
  
```
    cnpm i webuploader -S    
    cnpm i jquery -S  
    
```

2.  后台提供对应接口及参数，前端引入WebUpload.vue，后端参考：https://gitee.com/hhjNavy/webupload.git 前端参考：src/components/page/sys/FileList.vue

```
<template>
    <div>
        <div class="crumbs">
            <el-breadcrumb separator="/">
                <el-breadcrumb-item>
                    <i class="el-icon-lx-cascades"></i> 文件列表
                </el-breadcrumb-item>
            </el-breadcrumb>
        </div>

        <div class="container">
            <div class="handle-box">
               <div>
                   <el-input v-model="query.orgName" placeholder="文件文件名" class="handle-input mr10"></el-input>
                   <el-input v-model="query.fileType" placeholder="文件类型" class="handle-input mr10"></el-input>
                   <el-button type="primary" icon="el-icon-search" @click="handleSearch">搜索</el-button>
               </div>
                <div style="margin: 10px 0 -10px 0;">
                    <el-button
                            type="primary"
                            size="mini"
                            icon="el-icon-plus"
                            class="handle-del mr10"
                            @click="addAllSelection"
                    >上传文件</el-button>
                    <el-button
                            type="danger"
                            size="mini"
                            icon="el-icon-delete"
                            class="handle-del mr10"
                            @click="delAllSelection"
                    >批量删除</el-button>
                    <!--<el-button type="primary" size="mini" icon="el-icon-top" @click="showExportExcel">Excel导入</el-button>-->
                    <!--<input type="file" ref="clearFile" @change="readFile($event)" multiple="multiplt" class="add-file-right-input" style="margin-left:70px;" accept=".xlsx,.xls">-->
                </div>
            </div>
            <el-table
                    v-loading="loading"
                    element-loading-text="拼命加载中"
                    :data="tableData"
                    border
                    class="table"
                    ref="multipleTable"
                    header-cell-class-name="table-header"
                    @selection-change="handleSelectionChange"
            >

                <el-table-column type="expand">
                    <template slot-scope="props">
                        <el-form   label-width="110px" class="demo-table-expand">
                            <el-form-item label="ID">
                                <span>{{ props.row.id}}</span>
                            </el-form-item>
                            <el-form-item label="原文件名">
                                <span>{{ props.row.orgName }}</span>
                            </el-form-item>
                            <el-form-item label="生产文件名">
                                <span>{{ props.row.serverLocalName }}</span>
                            </el-form-item>
                            <el-form-item label="服务器储存路径">
                                <span>{{ props.row.serverLocalPath }}</span>
                            </el-form-item>
                            <el-form-item label="访问路径">
                                <el-link :href="props.row.networkPathURL" target="_blank">{{ props.row.networkPathURL }}</el-link>
                                <!--<span>{{ props.row.networkPathURL }}</span>-->
                            </el-form-item>
                            <el-form-item label="文件MD5值">
                                <span>{{ props.row.md5Value }}</span>
                            </el-form-item>
                            <el-form-item label="文件大小">
                                <span>{{ props.row.fileSizeStr }}</span>
                            </el-form-item>

                            <el-form-item label="文件类型">
                                <span>{{ props.row.fileType }}</span>
                            </el-form-item>

                            <el-form-item label="储存日期">
                                <span>{{ props.row.storageDate }}</span>
                            </el-form-item>

                            <el-form-item label="上传统计">
                                <span>{{ props.row.uploadCount }}</span>
                            </el-form-item>

                            <el-form-item label="下载统计">
                                <span>{{ props.row.downloadCount }}</span>
                            </el-form-item>


                            <el-form-item label="上传日期">
                                <span>{{ props.row.createTime }}</span>
                            </el-form-item>

                        </el-form>
                    </template>
                </el-table-column>

                <el-table-column type="selection" width="55" align="center"></el-table-column>
                <el-table-column prop="orgName"   label="文件名"></el-table-column>
                <el-table-column prop="fileSizeStr" label="文件大小"></el-table-column>
                <el-table-column prop="fileType" label="文件类型"></el-table-column>
                <el-table-column prop="createTime" width="180" label="上传日期"></el-table-column>

                <el-table-column label="操作" width="180" align="center"  fixed="right">
                    <template slot-scope="scope">
                        <el-button
                                type="text"
                                icon="el-icon-edit"
                                @click="seeFile(scope.$index, scope.row)"
                        >查看</el-button>
                        <el-button
                                type="text"
                                icon="el-icon-delete"
                                class="red"
                                @click="handleDelete(scope.$index, scope.row)"
                        >删除</el-button>
                    </template>
                </el-table-column>
            </el-table>
            <div class="pagination">
                <el-pagination
                        background
                        layout="total, sizes, prev, pager, next, jumper"
                        :current-page="query.pageIndex"
                        :page-size="query.pageSize"
                        :total="pageTotal"
                        @current-change="handlePageChange"
                        @size-change="handleSizeChange"
                ></el-pagination>
            </div>
        </div>

        <!-- 编辑弹出框 -->
        <el-dialog :title="formTitle" :visible.sync="editVisible"   width="800px">
            <WebUpload
                    :fileNumLimit="fileNumLimit"
                    :fileSize="fileSize"
                    :chunkSize="chunkSize"
                    :uploadSuffixUrl="uploadSuffixUrl"
                    :options="options"
                    :fileListData="fileList"
            > </WebUpload>
            <span slot="footer" class="dialog-footer">
                <!--<el-button @click="editVisible = false">取 消</el-button>-->
                <el-button type="primary" @click="saveEdit">确 定</el-button>
            </span>
        </el-dialog>

        <!--图片查看-->
        <el-dialog title="图片查看" :visible.sync="showImgVisible" width="800px">
            <el-image
                    v-if="imageUrl"
                    class="avatar"npm
                    :src="imageUrl"
                    :preview-src-list="[imageUrl]"
            ></el-image>
            <span slot="footer" class="dialog-footer">
                <!--<el-button @click="editVisible = false">取 消</el-button>-->
                <el-button type="primary" @click="showImgVisible=false;imageUrl=''">关 闭</el-button>
            </span>
        </el-dialog>

        <!--Excel导出-->

    </div>
</template>

<script>
    import { getList,delData,delDatas } from '../../../api/api';
    import WebUpload from '../../common/upload/WebUpload';

    export default {
        name: 'uploadfile',
        data() {
            return {
                query: {
                    orgName: '',
                    fileType: '',
                    pageIndex: 1,
                    pageSize: 10
                },
                formTitle:"添加",
                tableData: [],
                multipleSelection: [],
                delList: [],
                editVisible: false,
                pageTotal: 0,
                form: {},
                idx: -1,
                uId:0,
                id: -1,

                loading:true,
                uploadLoading: false,
                showRemoveFile: false,

                imageUrl:"",
                showImgVisible:false,
                uploader:{},
                uploadBtnDisabled:false,
                uploadStaus:"el-icon-upload",
                fileList:[],//显示集合
                fileNumLimit:200,//文件总数
                fileSize:100*1024*1024*1024,//默认上传文件大小
                chunkSize:5*1024*1024,//默认文件片段
                uploadSuffixUrl:"http://localhost:8080/",
                options:{
                    //允许上传的文件
                    fileType:"doc,docx,pdf,xls,xlsx,ppt,pptx,gif,jpg,jpeg,bmp,png,rar,zip,mp4,avi",
                    fileUploadUrl:"/upload/fileRecord/zone/upload",//上传地址
                    fileCheckUrl:"/upload/fileRecord/zone/upload/md5Check",//检测文件是否存在url
                    checkChunkUrl:"/upload/fileRecord/zone/upload/md5Check",//检测分片url
                    mergeChunksUrl:"/upload/fileRecord/zone/upload/merge/",//合并文件请求地址
                    headers:{}
                }

            };
        },
        components:{
            WebUpload
        },
        created() {
            this.getData();
        },
        methods: {
            getData() {
                var that=this;
                this.loading=true;
                var query=this.query;
                var pageSize=query.pageSize;
                var pageIndex=query.pageIndex;
                var reqData={};

                if(query.orgName.length>0){
                    reqData.orgName=query.orgName;
                }
                if(query.fileType!=null&&query.fileType!=""){
                    reqData.fileType=query.fileType;
                }

                getList(reqData,pageIndex,pageSize).then(res=>{
                    if(res.success){
                        var resdata=res.data;
                        var rows=resdata.rows;
                        for(var i=0;i<rows.length;i++){
                            var fileSize=rows[i].fileSize;
                            rows[i].fileSizeStr=this.formatFileSize(fileSize);
                            rows[i].networkPathURL=this.uploadSuffixUrl+ rows[i].networkPath;
                        }
                        that.tableData=rows;
                        that.pageTotal = resdata.total || 50;
                    }
                    that.loading=false;
                });
            },
            // 触发搜索按钮
            handleSearch() {
                this.$set(this.query, 'pageIndex', 1);
                this.getData();
            },
            // 删除操作
            handleDelete(index, row) {
                // 二次确认删除
                this.$confirm('确定要删除吗？', '提示', {
                    type: 'warning'
                })
                .then(() => {
                    delData(row.id).then(res=>{
                        if(res.success){
                            this.$message.success(res.message);
                            // this.tableData.splice(index, 1);
                            this.getData();
                        }else{
                            this.$message.error(res.message);
                        }
                    });
                })
                .catch(() => {
                    this.$message.success("取消成功");
                });
            },
            // 多选操作
            handleSelectionChange(val) {
                this.multipleSelection = val;
            },
            addAllSelection(){//添加数据
                this.form={
                    isFilter:'1',
                    contentType:'',
                    fileSuffix:'',
                    fileType:'',
                    fileTag:'',
                    fileDescribe:''
                };
                this.formTitle="上传文件";
                this.editVisible = true;
            },
            delAllSelection() {
                const length = this.multipleSelection.length;
                let str = '';
                let ids = '';
                if(length==0){
                    this.$message.error(`请选择数据`);
                    return;
                }
                this.delList = this.delList.concat(this.multipleSelection);
                for (let i = 0; i < length; i++) {
                    str += this.multipleSelection[i].orgName + '、';
                    ids += this.multipleSelection[i].id+',';
                }
                str=str.substring(0,str.length-1);
                ids=ids.substring(0,ids.length-1);
                delDatas(ids).then(res=>{
                    if(res.success){
                        this.$message.success(`删除了【${str}】`);
                        this.multipleSelection = [];
                        this.getData();
                    }else{
                        this.$message.error(res.message);
                    }
                });

            },
            // 编辑操作
            seeFile(index, row) {
                console.log("row:",row);
                var orgName=row.orgName;
                var fSuffix=orgName.substring(orgName.lastIndexOf(".")+1);
                if(fSuffix==="png"||fSuffix==="gif"
                    ||fSuffix==="jpg"||fSuffix==="jpeg"
                    ||fSuffix==="bmp"){
                    this.showImgVisible=true;
                    this.imageUrl=this.uploadSuffixUrl+row.networkPath;
                }else if(fSuffix==="mp4"||fSuffix==="mp3"||fSuffix==="pdf"){
                    this.$confirm('需要新开窗口页面，确定需要吗？', '提示', {
                        type: 'warning',
                        confirmButtonText: '确定',
                        cancelButtonText: '取消'
                    })
                    .then(() => {
                        window.open(this.uploadSuffixUrl+row.networkPath)
                    })
                    .catch(() => {
                        this.$message.success("取消成功");
                    });
                }else{
                    this.$confirm('不支持在线查看，请[下载]后查看', '提示', {
                        type: 'warning',
                        confirmButtonText: '下载',
                        cancelButtonText: '取消'
                    })
                    .then(() => {
                        window.open(this.uploadSuffixUrl+row.networkPath);
                    })
                    .catch(() => {
                        this.$message.success("取消成功");
                    });

                }
            },
            // 保存编辑
            saveEdit(formName) {
                console.log("form:",formName);
                this.editVisible=false;
                this.getData();
                this.fileList=[];

            },
            // 分页导航
            handlePageChange(val) {
                this.$set(this.query, 'pageIndex', val);
                this.getData();
            },
            handleSizeChange(val){
                this.$set(this.query, 'pageSize', val);
                this.getData();
            },
            handleGetFile(e){
                var file=e.target.files[0];
                console.log("file:",file);
                var type=file.type;
                var fileType=type.substring(0,type.lastIndexOf("/"));
                var fileName=file.name;
                var fileSuffix=fileName.substring(fileName.lastIndexOf(".")+1);
                this.form.contentType=type;
                this.form.fileType=fileType;
                this.form.fileSuffix=fileSuffix;
                console.log("this.form:",this.form);
            },
            formatFileSize(size){
                var fileSize =0;
                if(size/1024>1024){
                    var len = size/1024/1024;
                    fileSize = len.toFixed(2) +"MB";
                }else if(size/1024/1024>1024){
                    var len = size/1024/1024;
                    fileSize = len.toFixed(2)+"GB";
                }else{
                    var len = size/1024;
                    fileSize = len.toFixed(2)+"KB";
                }
                return fileSize;
            }
        }
    };
</script>

<style scoped>
    .handle-box {
        margin-bottom: 20px;
    }

    .handle-select {
        width: 120px;
    }

    .handle-input {
        width: 200px;
        display: inline-block;
    }
    .table {
        width: 100%;
        font-size: 14px;
    }
    .red {
        color: #ff0000;
    }
    .mr10 {
        margin-right: 10px;
    }
    .table-td-thumb {
        display: block;
        margin: auto;
        width: 40px;
        height: 40px;
    }

    .table-td-thumb-from-item {
        display: block;
        max-width: 200px;
        max-height: 200px;
    }


    .avatar {
        min-width: 100px;
        min-height: 100px;
        max-width: 80%;
        max-height: 80%;
        width: auto;
        height: auto;
        display: block;
        cursor: pointer;
        border: 1px solid #eee;
    }

    .el-image__inner{
        width: auto;
        height: auto;
    }

    .demo-table-expand {
        font-size: 0;
    }
    .demo-table-expand label {
        width: 90px;
        color: #99a9bf;
    }
    .demo-table-expand .el-form-item {
        margin-right: 0;
        margin-bottom: 0;
        width: 50%;
    }

</style>

```

#### 开发思路
参考后端的readme.md


````
 本人是一个后台开发的，前端写得不到位的地方，请多多指教
 使用过程中，遇到问题，请发邮件1573028099@qq.com或者加QQ1573028099，上班期间勿扰，谢谢
