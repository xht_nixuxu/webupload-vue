import Vue from 'vue';
import App from './App.vue';
import router from './router';
import ElementUI from 'element-ui';
import VueI18n from 'vue-i18n';
import Vuex from 'vuex'
import { messages } from './components/common/i18n';
import 'element-ui/lib/theme-chalk/index.css'; // 默认主题
import './assets/css/icon.css';
import './components/common/directives';
import 'babel-polyfill';


Vue.config.productionTip = false;
Vue.use(VueI18n);
Vue.use(ElementUI, {
    size: 'small'
});
Vue.use(Vuex);
const i18n = new VueI18n({
    locale: 'zh',
    messages
});



//使用钩子函数对路由进行权限跳转
router.beforeEach((to, from, next) => {
    document.title = `${to.meta.title} | webupload+vue+elementUI大文件分片上传演示`;
    next();

});


new Vue({
    router,
    i18n,
    render: h => h(App)
}).$mount('#app');
