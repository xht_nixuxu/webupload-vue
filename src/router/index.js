import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/filelist'
        },
        {
            path: '/',
            component: () => import(/* webpackChunkName: "home" */ '../components/common/Home.vue'),
            meta: { title: '自述文件' },
            children: [
                {
                    path: '/filelist',
                    component: () => import(/* webpackChunkName: "table" */ '../components/page/sys/FileList.vue'),
                    meta: { title: '文件列表' }
                },
                {
                    path: '/webuploader',
                    component: () => import(/* webpackChunkName: "table" */ '../components/page/sys/Upload.vue'),
                    meta: { title: '文件分片上传' }
                }
            ]
        },
        {
            path: '*',
            redirect: '/404'
        }
    ]
});
